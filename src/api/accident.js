import request from '@/utils/request';

/**
 * 分页查询关联项目事故列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getAccidentByUserId(data) {
  return request({
    url: '/project-api/accident/getAccidentByUserId',
    method: 'post',
    data
  });
}

/**
 *分页查询事故列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getAccidentList(data) {
  return request({
    url: '/project-api/accident/selectAccidentByPage',
    method: 'post',
    data
  });
}

/**
 *获取事故信息来源字典
 *
 * @export
 * @returns
 */
export function getAccidentInformation() {
  return request({
    url: '/project-api/accident/getAccidentInformationDictionary',
    method: 'post'
  });
}

/**
 *获取事故状态字典
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getAccidentState(data) {
  return request({
    url: '/project-api/accident/getAccidentStateDictionary',
    method: 'post',
    data
  });
}

/**
 *获取事故类型字典
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getAccidentType(data) {
  return request({
    url: '/project-api/accident/getAccidentTypeDictionary',
    method: 'post',
    data
  });
}

/**
 *新增事故
 *
 * @export
 * @param {*} data
 * @returns
 */
export function insertAccident(data) {
  return request({
    url: '/project-api/accident/insertAccident',
    method: 'post',
    data
  });
}
