import request from '@/utils/request';

/**
 * 审批抽查整改信息
 * @param {*} data 
 * @returns 
 */
export function approvalInspection(data) {
  return request({
    url: '/project-api/approval/approvalInspection',
    method: 'post',
    data
  })
}

/**
 * 审核维修维护报告
 *
 * @exporta
 * @param {*} data
 * @returns
 */
export function verifyMaintenance(data) {
  return request({
    url: '/project-api/approval/verifyMaintenance',
    method: 'post',
    data
  });
}

/**
 *审核人员审核
 *
 * @exporta
 * @param {*} data
 * @returns
 */
export function approvalVerify(data) {
  return request({
    url: '/project-api/approval/verify',
    method: 'post',
    data
  });
}

/**
 *各业务审批查看审批信息
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getApprovalInfo(data) {
  return request({
    url: '/project-api/approval/selectApprovalInfo',
    method: 'post',
    data
  });
}

/**
 *审批流配置
 *
 * @export
 * @param {*} data
 * @returns
 */
export function updateConfigUser(data) {
  return request({
    url: '/project-api/approval/updateConfigUser',
    method: 'post',
    data
  });
}

/**
 *分页查询审批流列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getPageConfigs(data) {
  return request({
    url: '/project-api/approval/selectPageConfigs',
    method: 'post',
    data
  });
}

/**
 *查询审批类型字典
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getApprovalTypes(data) {
  return request({
    url: '/project-api/approval/selectTypes',
    method: 'post',
    data
  });
}

/**
 *查询街道审批人员列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getApproverByDistrict(data) {
  return request({
    url: '/project-api/approval/selectUserByDistrict',
    method: 'post',
    data
  });
}
