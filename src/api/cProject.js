import request from '@/utils/request';
import { getToken } from '@/utils/auth';

/**
 * 批量导入项目资料
 *
 * @export
 * @returns
 */
export function importToExcel(data) {
  return request({
    url: '/project-api/curtain/project/importToExcel',
    method: 'post',
    data,
  });
}

/**
 * 导出项目资料
 *
 * @export
 * @returns
 */
export function exportPdf(data) {
  window.open(
    `${process.env.VUE_APP_BASE_API}/project-api/curtain/project/exportPdf?idProject=${data}&AccessToken=${getToken()}`,
    '_blank'
  );
}

/**
 *根据ID查询项目详情
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectList() {
  return request({
    url: '/project-api/curtain/project/selectById',
    method: 'post'
  });
}

/**
 *幕墙数据库(根据用户权限显示不同幕墙数据库列表)
 *
 * @export
 * @param {*} data
 * @returns
 */
export function curtainDatabase(data) {
  return request({
    url: '/project-api/curtain/project/selectProjectsByRole',
    method: 'post',
    data,
  });
}

/**
 * 查看当前账号提交的未入库项目信息
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectInfoByuser(data) {
  return request({
    url: '/project-api/curtain/project/selectByuser',
    method: 'post',
    data,
  });
}

/**
 *创建项目
 *
 * @export
 * @param {*} data
 * @returns
 */
export function insertProject(data, idProjectState, idProject) {
  return request({
    url: '/project-api/curtain/project/insertProject',
    method: 'post',
    data: {
      data,
      idProjectState,
      idProject
    },
  });
}

/**
 * 获取幕墙类型
 */
export function getCurtainType() {
  return request({
    url: '/project-api/curtain/curtainType/selectCurtainType',
    method: 'get'
  });
}

/**
 *幕墙数据库数据结构
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectField() {
    return request({
      url: '/project-api/curtain/project/selectProjectFiled',
      method: 'get'
    });
}

