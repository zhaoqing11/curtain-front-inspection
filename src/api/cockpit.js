import request from '@/utils/request';

/**
 *公告通知
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getNoticeInfo(data) {
  return request({
    url: '/project-api/cockpit/getNoticeInfo',
    method: 'post',
    data
  });
}

/**
 *幕墙玻璃类型
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getCurtainTypeInfo(data) {
  return request({
    url: '/project-api/cockpit/getCurtainTypeInfo',
    method: 'post',
    data
  });
}

/**
 *幕墙统计
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getCurtainStatisInfo(data) {
  return request({
    url: '/project-api/cockpit/getCurtainStatisInfo',
    method: 'post',
    data
  });
}

/**
 *报警统计
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getCurtainWarningInfo(data) {
  return request({
    url: '/project-api/cockpit/getCurtainWarningInfo',
    method: 'post',
    data
  });
}

/**
 *幕墙入库趋势
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getCurtainStatisInfoByMonth(data) {
  return request({
    url: '/project-api/cockpit/getCurtainStatisInfoByMonth',
    method: 'post',
    data
  });
}

/**
 *幕墙高度
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getCurtainHeightInfo(data) {
  return request({
    url: '/project-api/cockpit/getCurtainHeightInfo',
    method: 'post',
    data
  });
}

/**
 *幕墙使用年限
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getCurtainLifeInfo(data) {
  return request({
    url: '/project-api/cockpit/getCurtainLifeInfo',
    method: 'post',
    data
  });
}

/**
 *事故统计
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getCurtainAccidentInfo(data) {
  return request({
    url: '/project-api/cockpit/getCurtainAccidentInfo',
    method: 'post',
    data
  });
}

/**
 *幕墙信息列表统计
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectList(data) {
  return request({
    url: '/project-api/cockpit/getProjectList',
    method: 'post',
    data
  });
}

/**
 *查询用户所属区的相关信息
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getUserRegionInfo(data) {
  return request({
    url: '/project-api//cockpit/getUserRegionInfo',
    method: 'post',
    data
  });
}
