import request from '@/utils/request';

/**
 *新增阅读学习资料记录
 *
 * @export
 * @returns
 */
export function checkCurriculumRecipient(data) {
  return request({
    url: '/project-api/curriculum/checkCurriculumRecipient',
    method: 'post',
    data
  });
}

/**
 *获取学习资料阅读信息
 *
 * @export
 * @returns
 */
export function getReadCurriculumNum() {
  return request({
    url: '/project-api/curriculum/selectReadCurriculumNum',
    method: 'post'
  });
}

/**
 *分页查询课程列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getCurriculumList(data) {
  return request({
    url: '/project-api/curriculum/selectByPage',
    method: 'post',
    data
  });
}

/**
 *获取课程类型字典
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getCurriculumType() {
  return request({
    url: '/project-api/curriculum/getCurriculumType',
    method: 'post',
  });
}

/**
 *删除课程
 *
 * @export
 * @param {*} data
 * @returns
 */
export function deleteCurriculum(data) {
  return request({
    url: '/project-api/curriculum/delete',
    method: 'post',
    data
  });
}

