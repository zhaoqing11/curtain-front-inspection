import request from '@/utils/request';

/**
 * 获取平台登录次数及报告提交次数统计
 *
 * @export
 * @returns
*/
export function getCurtainDataAnalysis() {
    return request({
      url: '/project-api/dataAnalysis/getCurtainDataAnalysis',
      method: 'post'
    });
}

/**
 * 获取幕墙高度分类统计
 *
 * @export
 * @returns
*/
export function getCurtainHeightNum() {
    return request({
      url: '/project-api/dataAnalysis/getCurtainHeightNum',
      method: 'post'
    });
}

/**
 * 获取幕墙面积分类统计
 *
 * @export
 * @returns
*/
export function getCurtainTotalAreaNum() {
    return request({
      url: '/project-api/dataAnalysis/getCurtainTotalAreaNum',
      method: 'post'
    });
}

/**
 * 获取竣工使用时间分类统计
 *
 * @export
 * @returns
*/
export function getMainCompletionNum() {
    return request({
      url: '/project-api/dataAnalysis/getMainCompletionNum',
      method: 'post'
    });
}

/**
 * 获取建筑性质分类统计
 *
 * @export
 * @returns
*/
export function getHouseUseNum() {
    return request({
      url: '/project-api/dataAnalysis/getHouseUseNum',
      method: 'post'
    });
}

/**
 * 获取幕墙安全管理分类统计
 *
 * @export
 * @returns
*/
export function getCurtainTypeNum() {
    return request({
      url: '/project-api/dataAnalysis/getCurtainTypeNum',
      method: 'post'
    });
}

/**
 * 获取项目基础数据分析
 *
 * @export
 * @returns
 */
export function getDataInfo() {
  return request({
    url: '/project-api/dataAnalysis/getDataInfo',
    method: 'post'
  });
}