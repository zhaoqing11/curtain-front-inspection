import request from '@/utils/request';

/**
 *获取区域树
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getDistrictTree(data) {
  return request({
    url: '/project-api/district/getDistrictTree',
    method: 'post',
    data
  });
}

// 获取用户所属区域
export function selectByUser(data) {
  return request({
    url: '/project-api/district/selectByUser',
    method: 'post',
    data
  });
}


/**
 *根据父id获取区域
 *
 * @export
 * @param {*} data
 * @returns
 */
export function selectByParent(data) {
  return request({
    url: '/project-api/district/selectByParent',
    method: 'post',
    data
  });
}

/**
 *新增区域
 *
 * @export
 * @param {*} data
 * @returns
 */
export function insertDistrict(data) {
  return request({
    url: '/project-api/district/insertDistrict',
    method: 'post',
    data
  });
}

/**
 *修改区域
 *
 * @export
 * @param {*} data
 * @returns
 */
export function upadteDistrict(data) {
  return request({
    url: '/project-api/district/upadteDistrict',
    method: 'post',
    data
  });
}

/**
 *删除区域
 *
 * @export
 * @param {*} data
 * @returns
 */
export function deleteDistrict(data) {
  return request({
    url: '/project-api/district/deleteDistrict',
    method: 'post',
    data
  });
}

/**
 *获取区域树（带用户信息）
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getDistrictUserTree(data) {
  return request({
    url: '/project-api/district/getDistrictUserTree',
    method: 'post',
    data
  });
}

