import request from '@/utils/request';
import { getToken } from '@/utils/auth';


/**
 * 根据项目id获取关联应急备案资料
 *
 * @export
 * @param {*} data
 * @returns
 */
export function selectByProjectId(data) {
  return request({
    url: '/project-api/emergency/selectByProjectId',
    method: 'post',
    data,
  });
}

/**
 * 项目用户修改变更备案记录
 *
 * @export
 * @param {*} data
 * @returns
 */
export function updateHistory(data) {
  return request({
    url: '/project-api/emergency/updateEmergencyHistory',
    method: 'post',
    data,
  });
}

/**
 * 获取待审批备案信息
 *
 * @export
 * @param {*} data
 * @returns
 */
export function selectPendingApproval(data) {
  return request({
    url: '/project-api/emergency/selectPendingApproval',
    method: 'post',
    data,
  });
}

/**
 * 审批人员审批备案
 *
 * @export
 * @param {*} data
 * @returns
 */
export function filingApproval(data) {
  return request({
    url: '/project-api/emergency/filingApproval',
    method: 'post',
    data,
  });
}

/**
 * 监管用户查询项目备案信息详情
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getRecordDetail(data) {
  return request({
    url: '/project-api/emergency/getRecordDetail',
    method: 'post',
    data,
  });
}

/**
 * 监管用户查询项目备案信息待审核列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getRecordVerify(data) {
  return request({
    url: '/project-api/emergency/selectRecordVerifyByPage',
    method: 'post',
    data,
  });
}

/**
 * 根据备案号查询变更记录
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getEmergencyDetail(data) {
  return request({
    url: '/project-api/emergency/getEmergencyDetail',
    method: 'post',
    data,
  });
}

/**
 * 根据项目id查询变更记录
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getEmergencInfo(data) {
  return request({
    url: '/project-api/emergency/getEmergencInfo',
    method: 'post',
    data,
  });
}

/**
 * 新增变更备案记录
 *
 * @export
 * @param {*} data
 * @returns
 */
export function insertUpdateHistory(data) {
  return request({
    url: '/project-api/emergency/insertUpdateHistory',
    method: 'post',
    data,
  });
}

/**
 *获取备案状态
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getEmergencyState() {
  return request({
    url: '/project-api/emergency/getEmergencyState',
    method: 'post'
  });
}

