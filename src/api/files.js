import request from '@/utils/request';
import { getToken } from '@/utils/auth';

/**
 * 预览pdf
 * 
 * @param {*} id 
 */
export function readPDF(data) {
  window.open(
    `${process.env.VUE_APP_BASE_API}/project-api/file/get/${data.id}/${data.fileName}?AccessToken=${getToken()}`,
    '_blank'
  );
}

/**
 *获取上传备案文件列表信息
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getAccessoryByProject(data) {
  return request({
    url: '/project-api/accessory/getAccessoryByProject',
    method: 'post',
    data
  });
}

/**
 *获取文件信息列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getAccessoryByGroup(data) {
  return request({
    url: '/project-api/accessory/getAccessoryByGroup',
    method: 'post',
    data
  });
}

// 文件下载
export function downloadFile(id) {
  window.open(
    `${process.env.VUE_APP_BASE_API}/project-api/accessory/download?idAccessory=${id}&AccessToken=${getToken()}`,
    '_blank'
  );
}

