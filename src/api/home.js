import request from '@/utils/request';
import { getToken } from '@/utils/auth';

/**
 * 导出幕墙(建筑性质/幕墙面积/幕墙高度)统计数据分析
 *
 * @export
 * @returns
 */
export function exportCNAnalysisData(type) {
  window.open(
    `${process.env.VUE_APP_BASE_API}/project-api/index/exportCNAnalysisData?type=`+ type +`&AccessToken=${getToken()}`,
    '_blank'
  );
}

/**
 * 获取基础数据分析(建筑性质分布)
 *
 * @exportx
 * @returns
 */
export function getHouseUseData() {
  return request({
    url: '/project-api/index/getHouseUseData',
    method: 'post'
  });
}

/**
 * 获取数据分析（面积分布/幕墙高度/使用寿命）
 *
 * @exportx
 * @returns
 */
export function getAllDataInfo() {
  return request({
    url: '/project-api/index/getAllDataInfo',
    method: 'post'
  });
}

/**
 * 导出幕墙数据分析
 *
 * @export
 * @returns
 */
export function exportAnalysisData() {
  window.open(
    `${process.env.VUE_APP_BASE_API}/project-api/index/exportAnalysisData?AccessToken=${getToken()}`,
    '_blank'
  );
}

/**
 * 获取数据分析
 *
 * @exportx
 * @returns
 */
export function getAnalysisData() {
  return request({
    url: '/project-api/index/getDataInfo',
    method: 'post'
  });
}

/**
 * 监管人获取关联项目待办列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getPendingByProject(data) {
  return request({
    url: '/project-api/index/getPendingNumByProject',
    method: 'post',
    data
  });
}

/**
 *获取待办信息
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getPendingNum(data) {
  return request({
    url: '/project-api/index/getPendingNum',
    method: 'post',
    data
  });
}

/**
 *获取统计信息
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getDataInfo(data) {
  return request({
    url: '/project-api/statistical/getDataInfo',
    method: 'post',
    data
  });
}

