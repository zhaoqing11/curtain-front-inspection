import request from '@/utils/request';

/**
 * 获取抽查检查整改回复报告信息
 * @param {*} data 
 * @returns 
*/
export function getInspectionUpdateById(data) {
  return request({
    url: '/project-api/inspectionGroup/getInspectionUpdateById',
    method: 'post',
    data 
  });
}

/**
 * 获取待审核抽查整改列表
 * @param {*} data 
 * @returns 
 */
export function getInspectionUpdateList(data) {
  return request({
    url: '/project-api/inspectionGroup/selectInspectionUpdateList',
    method: 'post',
    data 
  });
}

/**
 * 获得项目关联监督检查列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getInspectionByIdUser(data) {
  return request({
    url: '/project-api/inspectionGroup/selectInspectionByIdUser',
    method: 'post',
    data
  });
}

/**
 * 新增阅读抽查检查记录
 *
 * @export
 * @param {*} data
 * @returns
 */
export function checkInspectionRecipient(data) {
  return request({
    url: '/project-api/inspectionGroup/checkInspectionRecipient',
    method: 'post',
    data
  });
}

/**
 * 获取抽查检查阅读信息
 *
 * @export
 * @returns
 */
export function getReadInspectionNum(data) {
  return request({
    url: '/project-api/inspectionGroup/selectReadInspectionNum',
    method: 'post',
    data
  });
}

/**
 *分页查询列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getInspectionList(data) {
  return request({
    url: '/project-api/inspectionGroup/selectByPage',
    method: 'post',
    data
  });
}

/**
 *新增抽查检查
 *
 * @export
 * @param {*} data
 * @returns
 */
export function insertInspection(data) {
  return request({
    url: '/project-api/inspectionGroup/insertInspection',
    method: 'post',
    data
  });
}

/**
 *抽查检查详情
 *
 * @export
 * @param {*} data
 * @returns
 */
export function selectInfoById(data) {
  return request({
    url: '/project-api/inspectionGroup/selectInfoById',
    method: 'post',
    data
  });
}

/**
 *提交抽查检查结果
 *
 * @export
 * @param {*} data
 * @returns
 */
export function changeInspectionResult(data) {
  return request({
    url: '/project-api/inspectionGroup/changeInspectionResult',
    method: 'post',
    data
  });
}

/**
 *抽查检查整改
 *
 * @export
 * @param {*} data
 * @returns
 */
export function addReformRecord(data) {
  return request({
    url: '/project-api/inspectionGroup/AddReformRecord',
    method: 'post',
    data
  });
}

/**
 *获取抽查检查状态
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getInspectionState(data) {
  return request({
    url: '/project-api/inspectionGroup/getInspectionStates',
    method: 'post',
    data
  });
}

