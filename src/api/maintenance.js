import request from '@/utils/request';

/**
 * 获取关联项目维修报告列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getMaintenanceByIdProject(data) {
  return request({
    url: '/project-api/patrolMaintenance/selectMaintenanceByIdProject',
    method: 'post',
    data
  });
}

/**
 *获取上报信息数据结构
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getReportField(data) {
  return request({
    url: '/project-api/patrolMaintenance/getReportField',
    method: 'post',
    data
  });
}

/**
 *根据ID获取上报信息详情
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getReportDetail(data) {
  return request({
    url: '/project-api/patrolMaintenance/selectReportById',
    method: 'post',
    data
  });
}

/**
 * 获取当前项目名称
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectName() {
  return request({
    url: '/project-api/patrolMaintenance/getProjectName',
    method: 'post'
  });
}

/**
 * 上传报告
 *
 * @export
 * @param {*} data
 * @returns
 */
export function insertReportMaintenance(data) {
  return request({
    url: '/project-api/patrolMaintenance/insertReportMaintenance',
    method: 'post',
    data
  });
}

/**
 * 获取报告类型列表
 *
 * @export
 * @returns
 */
export function getReportType() {
  return request({
    url: '/project-api/patrolMaintenance/getReportType',
    method: 'post'
  });
}

/**
 * 查询维护报告详情
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getMaintenanceById(data) {
  return request({
    url: '/project-api/patrolMaintenance/selectById',
    method: 'post',
    data
  });
}

/**
 * 获取巡查维护报告列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getMaintenanceList(data) {
  return request({
    url: '/project-api/patrolMaintenance/selectByPage',
    method: 'post',
    data
  });
}
