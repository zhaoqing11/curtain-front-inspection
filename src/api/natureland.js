import request from '@/utils/request';

/**
 *用地性质列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function selectNatureType(data) {
  return request({
    url: '/project-api/nature-land/selectNatureType',
    method: 'post',
    data
  });
}

