import request from '@/utils/request';


/**
 * 查询关联项目待办公告信息列表
 *
 * @export
 * @returns
 */
export function getNoticeByDone(data) {
  return request({
    url: '/project-api/notice/selectNoticeByDone',
    method: 'post',
    data
  });
}

/**
 *获取公告阅读信息
 *
 * @export
 * @returns
 */
export function selectReadNoticeNum(data) {
  return request({
    url: '/project-api/notice/selectReadNoticeNum',
    method: 'post',
    data
  });
}

/**
 *分页查询公告列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getNoticeList(data) {
  return request({
    url: '/project-api/notice/selectNoticeByPage',
    method: 'post',
    data
  });
}

/**
 *获取公告状态字典
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getNoticeState(data) {
  return request({
    url: '/project-api/notice/getNoticeStateDictionary',
    method: 'post',
    data
  });
}

/**
 *根据ID获取公告详情
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getNoticeDetail(data) {
  return request({
    url: '/project-api/notice/selectNoticeById',
    method: 'post',
    data
  });
}

/**
 *撤回公告
 *
 * @export
 * @param {*} data
 * @returns
 */
export function reNoticeById(data) {
  return request({
    url: '/project-api/notice/reNoticeById',
    method: 'post',
    data
  });
}

/**
 *发布公告
 *
 * @export
 * @param {*} data
 * @returns
 */
export function insertNotice(data) {
  return request({
    url: '/project-api/notice/insertNotice',
    method: 'post',
    data
  });
}

/**
 *确认公告已阅
 *
 * @export
 * @param {*} data
 * @returns
 */
export function checkNotice(data) {
  return request({
    url: '/project-api/notice/checkNotice',
    method: 'post',
    data
  });
}

