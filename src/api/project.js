import request from '@/utils/request';
import { getToken } from '@/utils/auth';



/**
 * 监管人查看与项目关联幕墙预警列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectByIdUser(data) {
  return request({
    url: '/project-api/project/selectProjectByIdUser',
    method: 'post',
    data,
  });
}

/**
 * 导出项目资料
 *
 * @export
 * @returns
 */
export function exportProjectExcel(data) {
  window.open(
    `${process.env.VUE_APP_BASE_API}/project-api/project/exportProjectExcel?idProject=${data}&AccessToken=${getToken()}`,
    '_blank'
  );
}

/**
 * 导出平台报表统计
 *
 * @export
 * @param {*} sheetName
 * @returns
 */
export function exportExcel(sheetName) {
  window.open(
    `${process.env.VUE_APP_BASE_API}/project-api/project/exportExcel?sheetName=${sheetName}&AccessToken=${getToken()}`,
    '_blank'
  );
}

/**
 * 获取平台及小程序数据分析统计
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getWeeklyReportStatistics(data) {
  return request({
    url: '/project-api/project/getWeeklyReportStatistics',
    method: 'post',
    data,
  });
}

/**
 * 预览pdf
 * 
 * @param {*} id 
 */
export function readPDF(id) {
  window.open(
    `${process.env.VUE_APP_BASE_API}/project-api/project/getPdf?idAccessory=${id}&AccessToken=${getToken()}`,
    '_blank'
  );
}

/**
 *根据ID查询项目详情
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectList(data) {
  return request({
    url: '/project-api/project/selectById',
    method: 'post',
    data,
  });
}

/**
 *创建项目
 *
 * @export
 * @param {*} data
 * @returns
 */
export function insertProject(data, idProjectState, idProject, idApproval, detailInfoStatus) {
  return request({
    url: '/project-api/project/insertProject',
    method: 'post',
    data: {
      data,
      idProjectState,
      idProject,
      idApproval,
      detailInfoStatus,
    },
  });
}

/**
 *删除项目
 *
 * @export
 * @param {*} data
 * @returns
 */
export function deleteProject(data) {
  return request({
    url: '/project-api/project/deleteByIds',
    method: 'post',
    data,
  });
}

/**
 *项目信息变更
 *
 * @export
 * @param {*} data
 * @returns
 */
export function updateProject(data) {
  return request({
    url: '/project-api/project/updateProject',
    method: 'post',
    data,
  });
}

/**
 *幕墙数据库(根据用户权限显示不同幕墙数据库列表)
 *
 * @export
 * @param {*} data
 * @returns
 */
export function curtainDatabase(data) {
  return request({
    url: '/project-api/project/selectProjectsByRole',
    method: 'post',
    data,
  });
}

/**
 *幕墙数据库数据结构
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectField(data) {
  return request({
    url: '/project-api/project/selectProjectFiled',
    method: 'post',
    data,
  });
}

/**
 *幕墙状态字典
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectState(data) {
  return request({
    url: '/project-api/project/selectProjectState',
    method: 'post',
    data,
  });
}

/**
 *幕墙类型字典
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectTypeState(data) {
  return request({
    url: '/project-api/project/selectProjectType',
    method: 'post',
    data,
  });
}

/**
 *项目创建者查看自己的项目信息
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectInfoByuser(data) {
  return request({
    url: '/project-api/project/selectByuser',
    method: 'post',
    data,
  });
}

/**
 *用户查询自己的项目变更记录
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectUpdateRecord(data) {
  return request({
    url: '/project-api/project/selectUpdateRecord',
    method: 'post',
    data,
  });
}

/**
 *审批人员查看待审批项目列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getProjectApproval(data) {
  return request({
    url: '/project-api/project/selectProjectApproval',
    method: 'post',
    data,
  });
}

/**
 *根据变更属性id返回属性信息
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getFieldInfoById(data) {
  return request({
    url: '/project-api/project/selectFieldInfoById',
    method: 'post',
    data,
  });
}

/**
 *根据审批id查看变更记录
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getUpdateByApproval(data) {
  return request({
    url: '/project-api/project/selectUpdateByApproval',
    method: 'post',
    data,
  });
}
