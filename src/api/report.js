import request from '@/utils/request';


/**
 * 获得项目关联年度信息上报列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getReportByIdUser(data) {
  return request({
    url: '/project-api/report/selectReportByIdUser',
    method: 'post',
    data
  });
}


/**
 *获取年度上报信息状态字典
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getReportState(data) {
  return request({
    url: '/project-api/report/getReportStateDictionary',
    method: 'post',
    data
  });
}

/**
 *获取年度上报信息数据结构
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getReportField(data) {
  return request({
    url: '/project-api/report/getReportField',
    method: 'post',
    data
  });
}

/**
 *分页查询年度上报信息列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getReportList(data) {
  return request({
    url: '/project-api/report/selectReportByPage',
    method: 'post',
    data
  });
}

/**
 *根据ID获取年度上报信息详情
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getReportDetail(data) {
  return request({
    url: '/project-api/report/selectReportById',
    method: 'post',
    data
  });
}

/**
 *新增年度上报信息
 *
 * @export
 * @param {*} data
 * @returns
 */
export function insertReport(data, idReportState, idReport) {
  return request({
    url: '/project-api/report/insertReport',
    method: 'post',
    data: {
      data,
      idReportState,
      idReport
    }
  });
}
