import request from '@/utils/request';

/**
 *分页查询文件列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getStatuteFileList(data) {
  return request({
    url: '/project-api/statuteFile/selectStatuteFileByPage',
    method: 'post',
    data
  });
}

/**
 *获取文件状态字典
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getStatuteFileState(data) {
  return request({
    url: '/project-api/statuteFile/getFileStateDictionary',
    method: 'post',
    data
  });
}

/**
 *根据ID获取文件详情
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getStatuteFileDetail(data) {
  return request({
    url: '/project-api/statuteFile/selectStatuteFileById',
    method: 'post',
    data
  });
}

/**
 *撤回文件
 *
 * @export
 * @param {*} data
 * @returns
 */
export function reStatuteFileById(data) {
  return request({
    url: '/project-api/statuteFile/reStatuteFileById',
    method: 'post',
    data
  });
}

/**
 *发布文件
 *
 * @export
 * @param {*} data
 * @returns
 */
export function insertStatuteFile(data) {
  return request({
    url: '/project-api/statuteFile/insertStatuteFile',
    method: 'post',
    data
  });
}

/**
 *确认文件已阅
 *
 * @export
 * @param {*} data
 * @returns
 */
export function checkStatuteFile(data) {
  return request({
    url: '/project-api/statuteFile/checkStatuteFile',
    method: 'post',
    data
  });
}

