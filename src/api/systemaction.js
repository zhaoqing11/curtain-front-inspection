import request from '@/utils/request';


/**
 * 获得项目关联用户登录网站记录
 *
 * @export
 * @returns
 */
export function getLoginCountByIdUser(data) {
  return request({
    url: '/user-api/systemTrafficStatistics/selectLoginCountByIdUser',
    method: 'post',
    data
  });
}

/**
 *新增用户行为
 *
 * @export
 * @returns
 */
export function insertUserAction(data) {
  return request({
    url: '/user-api/systemAction/insertSystemAction',
    method: 'post',
    data
  });
}

/**
 *根据id获取用户行为详情
 *
 * @export
 * @returns
 */
export function getSystemActionById(data) {
  return request({
    url: '/user-api/systemTrafficStatistics/getSystemActionById',
    method: 'post',
    data
  });
}

/**
 *获取用户行为记录列表
 *
 * @export
 * @returns
 */
export function getSystemActionList(data) {
  return request({
    url: '/user-api/systemTrafficStatistics/getSystemActionList',
    method: 'post',
    data
  });
}

/**
 * 记录用户登录系统时间
 *
 * @export
 * @returns
 */
export function insertSystemAction(data) {
  return request({
    url: '/user-api/systemTrafficStatistics/insert',
    method: 'post',
    data
  });
}

/**
 *修改用户行为
 *
 * @export
 * @returns
 */
export function updateSystemAction(data) {
  return request({
    url: '/user-api/systemTrafficStatistics/updateByPrimaryKey',
    method: 'post',
    data
  });
}

/**
 *删除用户行为
 *
 * @export
 * @returns
 */
export function deleteSystemAction(data) {
  return request({
    url: '/user-api/systemTrafficStatistics/deleteByPrimaryKey',
    method: 'post',
    data
  });
}

