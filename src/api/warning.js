import request from '@/utils/request';

/**
 *分页查询列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getWarningList(data) {
  return request({
    url: '/project-api/warning/selectWarningByPage',
    method: 'post',
    data
  });
}

/**
 *获取预警状态字典
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getWarningState(data) {
  return request({
    url: '/project-api/warning/getWarningStateDictionary',
    method: 'post',
    data
  });
}

/**
 *获取预警类型字典
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getWarningType(data) {
  return request({
    url: '/project-api/warning/getWarningTypeDictionary',
    method: 'post',
    data
  });
}

/**
 *项目预警变更
 *
 * @export
 * @param {*} data
 * @returns
 */
export function updateWarning(data) {
  return request({
    url: '/project-api/warning/updateWarning',
    method: 'post',
    data
  });
}

/**
 *分页查询项目预警变更列表
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getWarningHistroy(data) {
  return request({
    url: '/project-api/warning/selectWarningHistroy',
    method: 'post',
    data
  });
}

/**
 *预警阈值维护
 *
 * @export
 * @param {*} data
 * @returns
 */
export function updateWarningValue(data) {
  return request({
    url: '/project-api/warning/updateWarningValue',
    method: 'post',
    data
  });
}

