import request from '@/utils/request';

/**
 *获取天气情况
 *
 * @export
 * @param {*} data
 * @returns
 */
export function getWeatherInfo(data) {
  return request({
    url: '/project-api/weather/getInfo',
    method: 'get',
    data
  });
}

