import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

/* Layout */
import Layout from "@/layout";

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: "/login",
    component: () => import("@/views/login/index"),
    hidden: true
  },
  {
    path: "/registerAccount",
    component: () => import("@/views/login/registerAccount"),
    hidden: true
  },
  {
    path: "/404",
    component: () => import("@/views/404"),
    hidden: true
  },
  {
    path: "/",
    component: Layout,
    redirect: "/login",
    children: [
      {
        path: "home",
        name: "Home",
        component: () => import("@/views/home/index"),
        meta: { title: "首页", icon: "home", requireAuth: true }
      }
    ]
  },
  {
    path: "/",
    component: Layout,
    redirect: "/cockpit",
    children: [
      {
        path: "cockpit",
        name: "Cockpit",
        component: () => import("@/views/cockpit/index"),
        meta: { title: "驾驶舱", icon: "cockpit", requireAuth: true }
      }
    ]
  },
  {
    path: "/setting",
    component: Layout,
    redirect: "/setting",
    name: "Setting",
    meta: { title: "系统设置", icon: "cog", requireAuth: true },
    children: [
      {
        path: "userManagement",
        name: "UserManagement",
        component: () => import("@/views/setting/userManagement"),
        meta: { title: "用户管理", icon: "peoples", requireAuth: true }
      },
      {
        path: "roleManagement",
        name: "RoleManagement",
        component: () => import("@/views/setting/roleManagement"),
        meta: { title: "角色管理", icon: "role", requireAuth: true }
      },
      {
        path: "areaTree",
        name: "AreaTree",
        component: () => import("@/views/setting/areaTree"),
        meta: { title: "区域层级树维护", icon: "tree", requireAuth: true }
      },
      {
        path: "warning",
        name: "Warning",
        component: () => import("@/views/setting/warning"),
        meta: { title: "预警阈值维护", icon: "warning-2", requireAuth: true }
      },
      {
        path: "approval",
        name: "Approval",
        component: () => import("@/views/setting/approval"),
        meta: { title: "审批流配置", icon: "approval", requireAuth: true }
      }
    ]
  },
  {
    path: "/register",
    component: Layout,
    redirect: "/register",
    name: "Register",
    meta: { title: "注册管理", icon: "register-2", requireAuth: true },
    children: [
      {
        path: "project",
        component: () => import("@/views/register/project/index"),
        meta: { title: "项目信息", icon: "list", requireAuth: true },
        children: [
          {
            path: "",
            name: "Project",
            redirect: "projectInfo",
            hidden: true
          },
          {
            path: "projectInfo",
            name: "ProjectInfo",
            component: () => import("@/views/register/project/projectInfo"),
            meta: { title: "项目信息", requireAuth: true },
            hidden: true
          },
          {
            path: "changeManagement",
            name: "ChangeManagement",
            component: () => import("@/views/register/project/changeManagement"),
            meta: { title: "变更管理", requireAuth: true },
            hidden: true
          }
        ]
      },
      {
        path: "infoApproval",
        name: "infoApproval",
        component: () => import("@/views/register/infoApproval"),
        meta: { title: "注册信息审核", icon: "info-approval", requireAuth: true }
      },
      {
        path: "approvalDetail",
        name: "approvalDetail",
        component: () => import("@/views/register/approvalDetail"),
        meta: { title: "注册信息详情", icon: "info-approval", requireAuth: true }
      },
      {
        path: "changeEidt",
        name: "ChangeEidt",
        component: () => import("@/views/register/project/changeEdit"),
        meta: { title: "新的项目信息修改", requireAuth: true },
        hidden: true
      },
      {
        path: "emergency",
        component: () => import("@/views/register/emergency/index"),
        meta: { title: "应急预案备案", icon: "list", requireAuth: true },
        children: [
          {
            path: "",
            name: "Emergency",
            redirect: "emergencyFiling",
            hidden: true
          },
          {
            path: "emergencyFiling",
            name: "EmergencyFiling",
            component: () => import("@/views/register/emergency/emergencyFiling"),
            meta: { title: "应急预案备案", icon: "report-up", requireAuth: true },
          },
          {
            path: "changeEmergencyMnt",
            name: "ChangeEmergencyMnt",
            component: () => import("@/views/register/emergency/changeEmergencyMnt"),
            meta: { title: "变更管理", icon: "report-up", requireAuth: true },
          }
        ]
      },
      {
        path: "changeEidtEmergency",
        name: "ChangeEidtEmergency",
        component: () => import("@/views/register/emergency/changeEdit"),
        meta: { title: "新的备案信息修改", requireAuth: true },
        hidden: true
      },
      {
        path: "infoRecordVerify",
        name: "infoRecordVerify",
        component: () => import("@/views/register/infoRecordVerify"),
        meta: { title: "备案信息审核", icon: "info-approval", requireAuth: true }
      },
      {
        path: "recordVerifyDetail",
        name: "recordVerifyDetail",
        component: () => import("@/views/register/recordVerifyDetail"),
        meta: { title: "备案信息详情", icon: "info-approval", requireAuth: true }
      }
    ]
  },
  {
    path: "/curtainManagement",
    component: Layout,
    redirect: "/curtainManagement",
    name: "CurtainManagement",
    meta: { title: "幕墙数据管理", icon: "excel", requireAuth: true },
    alwaysShow: true,
    children: [
      {
        path: "curtain",
        name: "Curtain",
        component: () => import("@/views/curtainManagement/curtain"),
        meta: { title: "幕墙数据库", icon: "database", requireAuth: true }
      },
      {
        path: "curtainDetail",
        name: "CurtainDetail",
        component: () => import("@/views/curtainManagement/curtainDetail"),
        meta: { title: "幕墙数据库详情", requireAuth: true },
        hidden: true
      }
    ]
  },
  {
    path: "/registerNotice",
    component: Layout,
    redirect: "/registerNotice",
    name: "RegisterNotice",
    meta: { title: "注册须知", icon: "excel", requireAuth: true },
    alwaysShow: true,
    children: [
      {
        path: "onlineLearning",
        name: "OnlineLearning",
        component: () => import("@/views/registerNotice/onlineLearning"),
        meta: { title: "在线学习", icon: "database", requireAuth: true }
      }
    ]
  },
  {
    path: "/curtainWarning",
    component: Layout,
    redirect: "/curtainWarning",
    name: "CurtainWarning",
    meta: { title: "幕墙预警", icon: "warning-early", requireAuth: true },
    alwaysShow: true,
    children: [
      {
        path: "warningList",
        name: "WarningList",
        component: () => import("@/views/curtainWarning/warningList"),
        meta: { title: "检查鉴定预警列表", icon: "list", requireAuth: true }
      },
      {
        path: "warningDetail",
        name: "warningDetail",
        component: () => import("@/views/curtainWarning/warningDetail"),
        meta: { title: "检查鉴定预警详情", requireAuth: true },
        hidden: true
      },
      {
        path: "inspection",
        component: () => import("@/views/curtainWarning/inspection/index"),
        meta: { title: "监督抽查整改预警", icon: "list", requireAuth: true },
      }
    ]
  },
  {
    path: "/accidentManagement",
    component: Layout,
    redirect: "/accidentManagement",
    name: "AccidentManagement",
    meta: { title: "事故管理", icon: "warning-3", requireAuth: true },
    alwaysShow: true,
    children: [
      {
        path: "accidentReport",
        name: "AccidentReport",
        component: () => import("@/views/accidentManagement/accidentReport"),
        meta: { title: "事故列表", icon: "report-up", requireAuth: true }
      },
      {
        path: "addAccident",
        name: "AddAccident",
        component: () => import("@/views/accidentManagement/addAccident"),
        meta: { title: "新增事故上报", requireAuth: true },
        hidden: true
      },
      {
        path: "accidentDetail",
        name: "AccidentDetail",
        component: () => import("@/views/accidentManagement/accidentDetail"),
        meta: { title: "事故详情", requireAuth: true },
        hidden: true
      }
    ]
  },
  {
    path: "/noticeManagement",
    component: Layout,
    redirect: "/noticeManagement",
    name: "NoticeManagement",
    meta: { title: "文件管理", icon: "icon-notice", requireAuth: true },
    children: [
      {
        path: "notice",
        name: "Notice",
        component: () => import("@/views/noticeManagement/notice"),
        meta: { title: "公告通知", icon: "notice", requireAuth: true }
      },
      {
        path: "addNotice",
        name: "addNotice",
        component: () => import("@/views/noticeManagement/addNotice"),
        meta: { title: "新增公告通知", icon: "notice", requireAuth: true }
      },
      {
        path: "noticeDetail",
        name: "NoticeDetail",
        component: () => import("@/views/noticeManagement/noticeDetail"),
        meta: { title: "公告详情", icon: "notice", requireAuth: true }
      },
      {
        path: "inspection",
        name: "Inspection",
        component: () => import("@/views/noticeManagement/inspection"),
        meta: { title: "抽查检查", icon: "inspection", requireAuth: true }
      },
      {
        path: "inspectionUpdate",
        name: "InspectionUpdate",
        component: () => import("@/views/noticeManagement/inspectionUpdateApproval"),
        meta: { title: "抽查整改审核", icon: "inspectionUpdate", requireAuth: true }
      },
      {
        path: "addInspection",
        name: "AddInspection",
        component: () => import("@/views/noticeManagement/addInspection"),
        meta: { title: "新增抽查检查", icon: "inspection", requireAuth: true },
        hidden: true
      },
      {
        path: "statuteFile",
        name: "StatuteFile",
        component: () => import("@/views/noticeManagement/statuteFile"),
        meta: { title: "法规文件", icon: "statuteFile", requireAuth: true }
      },
      {
        path: "addStatuteFile",
        name: "addStatuteFile",
        component: () => import("@/views/noticeManagement/addStatuteFile"),
        meta: { title: "新增文件", icon: "statuteFile", requireAuth: true }
      },
      {
        path: "statuteFileDetail",
        name: "StatuteFileDetail",
        component: () => import("@/views/noticeManagement/statuteFileDetail"),
        meta: { title: "文件详情", icon: "statuteFile", requireAuth: true }
      }
    ]
  },

  {
    path: "/annualReportMangement",
    component: Layout,
    redirect: "/annualReportMangement",
    name: "AnnualReport",
    meta: { title: "年度信息上报管理", icon: "checklist", requireAuth: true },
    alwaysShow: true,
    children: [
      {
        path: "annualReport",
        name: "AnnualReport",
        component: () => import("@/views/annualReportMangement/annualReport"),
        meta: { title: "年度信息上报", icon: "list", requireAuth: true }
      },
      {
        path: "addAnnualReport",
        name: "AddAnnualReport",
        component: () => import("@/views/annualReportMangement/addAnnualReport"),
        meta: { title: "新增年度上报信息", icon: "list", requireAuth: true },
        hidden: true
      }
    ]
  },

  {
    path: "/systemMonitoring",
    component: Layout,
    redirect: "/systemMonitoring",
    name: "SystemMonitoring",
    meta: { title: "系统监管", icon: "warning-3", requireAuth: true },
    alwaysShow: true,
    children: [
      {
        path: "systemActionView",
        name: "SystemActionView",
        component: () => import("@/views/systemMonitoring/systemActionView"),
        meta: { title: "用户行为", icon: "report-up", requireAuth: true }
      },
      {
        path: "systemActionDetail",
        name: "SystemActionDetail",
        component: () => import("@/views/systemMonitoring/systemActionDetail"),
        meta: { title: "用户行为详情", icon: "report-up", requireAuth: true }
      }
    ]
  },

  {
    path: "/reportManager",
    component: Layout,
    redirect: "/reportManager",
    name: "ReportManager",
    meta: { title: "统计管理", icon: "warning-3", requireAuth: true },
    alwaysShow: true,
    children: [
      {
        path: "reportStatisticsView",
        name: "ReportStatisticsView",
        component: () => import("@/views/reportManager/reportStatisticsView"),
        meta: { title: "报表统计", icon: "report-up", requireAuth: true }
      },
      {
        path: "reportRegularView",
        name: "ReportRegularView",
        component: () => import("@/views/reportManager/reportRegularView"),
        meta: { title: "定期报告", icon: "report-up", requireAuth: true }
      },
      {
        path: "weekDataAnalysis",
        name: "WeekDataAnalysis",
        component: () => import("@/views/reportManager/weekDataAnalysis"),
        meta: { title: "周报统计", icon: "report-up", requireAuth: true }
      }
    ]
  },

  {
    path: "/patrolMaintenance",
    component: Layout,
    redirect: "/patrolMaintenance",
    name: "PatrolMaintenance",
    meta: { title: "日常巡查维护", icon: "icon-patrol", requireAuth: true },
    children: [
      {
        path: "reportMaintenanceInfo",
        name: "reportMaintenanceInfo",
        component: () => import("@/views/patrolMaintenance/reportMaintenanceInfo"),
        meta: { title: "维修维护报告列表", icon: "patrolMaintenance", requireAuth: true }
      },
      // {
      //   path: "addReportMaintenance",
      //   name: "addReportMaintenance",
      //   component: () => import("@/views/patrolMaintenance/addReportMaintenance"),
      //   meta: { title: "日常巡查报告", icon: "patrolMaintenance", requireAuth: true }
      // },
      {
        path: "maintenanceDetail",
        name: "maintenanceDetail",
        component: () => import("@/views/patrolMaintenance/maintenanceDetail"),
        meta: { title: "查看维护报告", icon: "patrolMaintenance", requireAuth: true }
      },
      {
        path: "addMaintenanceReport",
        name: "addMaintenanceReport",
        component: () => import("@/views/patrolMaintenance/addMaintenanceReport"),
        meta: { title: "上传维护报告", icon: "patrolMaintenance", requireAuth: true }
      }
    ]
  }
];

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
// export const asyncRoutes = [

//   // 404 page must be placed at the end !!!
//   { path: '*', redirect: '/404', hidden: true }
// ]

const createRouter = () =>
  new Router({
    // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  });

const router = createRouter();

// export function resetRouter() {
//   const newRouter = createRouter()
//   router.matcher = newRouter.matcher // reset router
// }

export default router;
