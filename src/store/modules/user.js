import { LoginByUsername, logout, getClientIpAddress } from "@/api/user";
import * as AUTH from "@/utils/auth";
import * as API from "@/api/role";

const user = {
  state: {
    token: AUTH.getToken(),
    name: AUTH.getName(),
    avatar: "",
    roles: [],
    clientIpInfo: {},
    password: ""
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token;
    },
    SET_NAME: (state, name) => {
      state.name = name;
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar;
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles;
    },
    SET_CLIENTIPINFO: (state, clientIpInfo) => {
      state.clientIpInfo = clientIpInfo;
    },
    SET_PASSWORD: (state, password) => {
      state.password = password;
    }
  },

  actions: {
    LoginByUsername({ commit }, userInfo) {
      const { user, password, tryCode, jpgKey } = userInfo; // phone, checkCode, 
      return new Promise((resolve, reject) => {
        // telephone: phone.trim(),
        // checkCode,
        LoginByUsername({
          phone: user.trim(),
          password: password.trim(),
          tryCode,
          jpgKey
        }).then(res => {
            if (res.data.status === 200) {
              // 记录用户ip
              getClientIpAddress({
                userId: res.data.datas.idUser,
                phone: null // phone
              }).then(res => {
                AUTH.removeClientIpInfo()
                AUTH.setClientIpInfo(res.data.datas)
              })

              const tmpData = res.data.datas;
              AUTH.setName(tmpData.realName);
              AUTH.setRole(tmpData.idRole);
              AUTH.setUserId(tmpData.idUser);
              AUTH.setToken(tmpData.accessToken);
              const param = {
                idRole: tmpData.idRole
              };
              API.getAuthByRole(param).then(res => {
                if (res.data.status === 200) {
                  const tmpData = res.data.datas;
                  AUTH.setPerimission(tmpData);
                }
                resolve(res);
              });
            } else {
              resolve(res);
            }
          })
          .catch(error => {
            reject(error);
          });
      });
    },

    // user logout
    logout({ commit, state }) {
      return new Promise((resolve, reject) => {
        logout(state.token)
          .then(res => {
            AUTH.removeName();
            AUTH.removeRole();
            AUTH.removeToken();
            AUTH.removeUserId();
            AUTH.removeClientIpInfo();
            resolve(res);
          })
          .catch(error => {
            reject(error);
          });
      });
    }
  }
};

export default user;
